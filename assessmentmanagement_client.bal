
//import ballerina/grpc;
import ballerina/log;
import ballerina/io;
//import ballerina/http;

//assessmentManagementClient ep = check new ("http://localhost:9090");

function displayOptions(){
    io:println("Please select a number below according to what you wish to do today:");
    io:println("1. Create User");
    io:println("2. Create Course");
    io:println("3. create_assessment");
    io:println("4. Assign Course to an Assessor");
    io:println("5. Register for the Course");
    io:println("6. Submit Assignment");
    io:println("7. view_results");
    io:println("8. Request Assignment");
    io:println("9. Submit Marks");
    io:println("10. Exit");
    string choice = io:readln("Enter Choice: ");

    if (choice == "1"){
        create_users();
    }
    else if (choice == "2"){
        create_courses();
    }
    else if (choice == "3"){
        create_assessments();
    }
    else if (choice == "4"){
        assign_courses();
    }
    else if(choice == "5"){
        register();
    }
    else if(choice == "6"){
        submit_assignments();
    }
    else if (choice == "7"){
        view_results();
    }
    else if(choice == "8"){
        request_assignments();
    }
    else if(choice == "9"){
        submit_marks();
    }
    else if(choice == "10"){
        exit();
    }
    else {
        io:println("Error: Incorrect Input");
        displayOptions();
    }
}
function create_users () {
    log:printInfo("**********************************************************************");
    log:printInfo("----------------------------Creating User-----------------------------");
    log:printInfo("**********************************************************************");

    map<json> newUser= {
        sNumber: "",
        name: ""
    };

    string user_number = io:readln("Enter user Student/Staff number: ");
    newUser["sNumber"] = user_number;

    string user_name = io:readln("Enter user name: ");
    newUser["name"] = user_name;

    io:println("The newly created user is: ", newUser.toJsonString());
    displayOptions();  
}

function create_courses () {
    log:printInfo("**********************************************************************");
    log:printInfo("--------------------------Creating a Course---------------------------");
    log:printInfo("**********************************************************************");

    map<json> newCourse= {
        courseCode: "",
        courseName: ""
    };

    string course_code = io:readln("Enter the course code: ");
    newCourse["courseCode"] = course_code;

    string course_name = io:readln("Enter the course name: ");
    newCourse["courseName"] = course_name;

    //int|error totalAssignmentsEr = 'int:fromString(io:readln("Enter total number of Assignments of this Course: "));
    //int totalAssignments = check <int> totalAssignmentsEr;

    //json[] assignment = <json[]>newCourse["assignments"];
    //foreach int i in 0..<totalAssignments{
       // string description = io:readln("Enter description of this assignment: ");
        //var weight = io:readln("Enter weight of this assignment: ");
        //Write to the map 
       // assignment[i] = {assignmentName: description, assignmentWeight: weight};
    //}
    //newCourse["assignments"] = assignment;

    //Sending the create requests 
    //assessment_management_systemBlockingClient blockingEp = new(port);
    
   // var create_courses = blockingEp->createCourse(newCourse);
   // if (create_courses is grpc:Error) {
   //     io:println("Error from Connector: " + create_courses.reason() + " - "
   //                                             + <string>create_courses.detail()["message"] + "\n");
   // } else {

   //         io:println("course Succesfully created: ",create_courses);
       
   // }

    io:println("The newly created Course is: ", newCourse.toJsonString());
    displayOptions();  
}
function create_assessments () {
    log:printInfo("**********************************************************************");
    log:printInfo("------------------------Creating assessments--------------------------");
    log:printInfo("**********************************************************************");

    map<json> newAssessment= {
        quantity: "",
	    assignment_id: "",
	    weight: ""
    };

    string quantity = io:readln("Enter the assignment number of this Course: ");
    newAssessment["totalAssignments"] = quantity;

    string assignment_id = io:readln("Enter the assignment name: ");
    newAssessment["assignmentName"] = assignment_id;

    string weight = io:readln("Enter the assignment weight: ");
    newAssessment["assignmentName"] = weight;

    io:println("The newly created assessment is: ", newAssessment.toJsonString());
    displayOptions();  
}

function assign_courses () {
    log:printInfo("**********************************************************************");
    log:printInfo("------------------Assigning Course to an Assessor---------------------");
    log:printInfo("**********************************************************************");

    map<json> newAssessor= {
        courseCode: "",
        courseAssessor: "",
        sNumber: "",
        name: ""
    };

    string course_code = io:readln("Enter the course code: ");
    newAssessor["courseCode"] = course_code;

    string courseAssessor = io:readln("Enter the position of an Assessor assigned to this Course: ");
    newAssessor ["assessor"] = courseAssessor;

    
    string sNumber = io:readln("Enter Course Assessor Staff Number: ");
    newAssessor["staffNumber"] = sNumber;

    string name = io:readln("Enter Course Assessor Name: ");
    newAssessor["assessorName"] = name;

    io:println("This Course is assigned to: ", newAssessor.toJsonString());
    displayOptions();
}

function register () {
    log:printInfo("**********************************************************************");
    log:printInfo("------------------------Register your Course--------------------------");
    log:printInfo("**********************************************************************");

    map<json> newRegister= {
        sNumber: "",
        courseCode: ""
    };

    string student_number = io:readln("Enter your Student number: ");
    newRegister["sNumber"] = student_number;

    string course_code = io:readln("Enter the Course code you want to register: ");
    newRegister["courseCode"] = course_code;

    io:println("You have successfully enrolled in: ", newRegister.toJsonString());
    displayOptions();  
}

function submit_assignments () {
    log:printInfo("**********************************************************************");
    log:printInfo("--------------------------Submit Assignment---------------------------");
    log:printInfo("**********************************************************************");

    map<json> newSubmitAssignment= {
        sNumber: "",
        description: "",
        course_code: ""
    };

    var student_number = io:readln("Enter your Student number: ");
    newSubmitAssignment["sNumber"] = student_number;

    string assignment_name = io:readln("Enter assignment name: ");
    newSubmitAssignment["description"] = assignment_name;

    string course_code = io:readln("Enter Course code: ");
    newSubmitAssignment["courseCode"] = course_code;

    io:println("You have successfully submitted: ", newSubmitAssignment.toJsonString());
    displayOptions();  
}

function view_results () {
    log:printInfo("**********************************************************************");
    log:printInfo("---------------------viewing assignments results-----------------------");
    log:printInfo("**********************************************************************");

    map<json> newResult= {
        assignment_id: "",
        marks: ""
    };

    string assignment_id = io:readln("Enter the assignment Id: ");
    newResult["assignmentId"] = assignment_id;

    io:println("You are retrieving results of: ", newResult.toJsonString());
    displayOptions();  
}

function request_assignments () {
    log:printInfo("**********************************************************************");
    log:printInfo("------------------------Request An Assignment-------------------------");
    log:printInfo("**********************************************************************");

    map<json> newRequestAssignment= {
        description: "",
        course_code: ""
    };

    string description = io:readln("Enter assignment name: ");
    newRequestAssignment["assignment_name"] = description;

    string course_code = io:readln("Enter assignment course code: ");
    newRequestAssignment["courseName"] = course_code;

    io:println("You have successfully retrieved: ", newRequestAssignment.toJsonString());
    displayOptions();  
}

function submit_marks () {
    log:printInfo("**********************************************************************");
    log:printInfo("-----------------------Submit Assignment Marks------------------------");
    log:printInfo("**********************************************************************");

    map<json> newSubmitMark= {
        sNumber: "",
        assignmentMark: "",
        description: "",
        courseCode: ""
    };

    string student_number = io:readln("Enter Student number: ");
    newSubmitMark["sNumber"] = student_number;

    string assignment_mark = io:readln("Enter assignment mark: ");
    newSubmitMark["assignmentMark"] = assignment_mark;

    string assignment_name = io:readln("Enter assignment name: ");
    newSubmitMark["description"] = assignment_name;

    string course_code = io:readln("Enter Course code: ");
    newSubmitMark["courseCode"] = course_code;

    io:println("You have successfully submitted: ", newSubmitMark.toJsonString());
    displayOptions();  
}

function exit () {
    io:println("*******************GOOD BYE!!***********************");
}

public function main (string... args) {

    
    io:println("*****************************************************************************");
    io:println("*****************************************************************************");
    io:println("                     WELCOME TO AN ASSESSMENT MANAGEMENT SYSTEM");
    io:println("*****************************************************************************");
    io:println("*****************************************************************************");
    displayOptions();
}

