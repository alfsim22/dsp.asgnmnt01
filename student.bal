//import ballerina/io;
import ballerina/http;
import ballerina/log;
import ballerina/uuid;
 
 // student record
public type Student record {|
  
   string student_number?;
   string name;
   string email;
   Course course;
|};
// object st1
Student st1 ={
    student_number: "232323",
    name: "Jesicca Miyano",
    email: "mjesicca@gmail.com",
    course: {course_code: "DSA621S", weight: 100, mark: 88}
};
 
 // record Course
public type Course record {|
   string course_code;
   decimal weight;
   decimal mark;
|};
// object cours1
Course cours1 ={
    course_code: "dsp621",
    weight: 100,
    mark: 90
};
 
// an error
public type Error record {|
   string code;
   string message;
|};
 
// Error response
public type ErrorResponse record {|
   Error 'error;
|};
 
// Bad request response
public type ValidationError record {|
   *http:BadRequest;
   ErrorResponse body;
|};
 
// headers of created response
public type LocationHeader record {|
   // Location header. A link to the created student.
   string location;
|};
 
//Student Created response
public type StudentCreated record {|
   *http:Created;
   // Location header representing a link to the created student.
   LocationHeader headers;
|};
 
// Student updated response
public type StudentUpdated record {|
   *http:Ok;
|};

// course details respnse
public type CourseUpdated record {|
   *http:Ok;
|};
 
// The student service
service / on new http:Listener(9090) {
 
   private map<Student> students = {};
   private map<Course> courses = {};
 
   // fetch all students
   resource function get students() returns Student[] {
       return self.students.toArray();
   }
   
   // lookup a single student
   resource function get student (string student_number) returns Student [] {
        return self.students.toArray();
    }
    
   // list all courses
   resource function get courses() returns Course[] {
       return self.courses.toArray();
   }
 
   // Create a new student
   // return - student created response or validation error
   resource function post students(@http:Payload Student student) returns StudentCreated|ValidationError {
       if student.name.length() == 0 || student.email.length() == 0 {
           log:printWarn("Student name or email is not present", student = student);
           return <ValidationError>{
               body: {
                   'error: {
                       code: "INVALID_NAME",
                       message: "Student name and email are required"
                   }
               }
           };
       }
 
       if student.course.weight < 0d {
           log:printWarn("Student course weight cannot be negative", student = student);
           return <ValidationError>{
               body: {
                   'error: {
                       code: "INVALID_WEIGHT",
                       message: "Student course weight cannot be negative"
                   }
               }
           };
       }
 
       log:printDebug("Creating new student", student = student);
       student.student_number = uuid:createType1AsString();
       self.students[<string>student.student_number] = student;
       log:printInfo("Created new student", student = student);
 
       string studentUrl = string `/students/${<string>student.student_number}`;
       return <StudentCreated>{
           headers: {
               location: studentUrl
           }
       };
   }
 
   // Update student details
   // return - A student updated response or an error if student is invalid
   resource function put student(@http:Payload Student student) returns StudentUpdated|ValidationError {
       if student.student_number is () || !self.students.hasKey(<string>student.student_number) {
           log:printWarn("Invalid student provided for update", student = student);
           return <ValidationError>{
               body: {
                   'error: {
                       code: "INVALID_STUDENT",
                       message: "Invalid student"
                   }
               }
           };
       }
 
       log:printInfo("Updating student", student = student);
       self.students[<string>student.student_number] = student;
       return <StudentUpdated>{};
   }
    
    // Update student’s course details
    // return - A student’s course details updated response or an error if student’s course details is invalid
    resource function put course(@http:Payload Course course) returns CourseUpdated|ValidationError {
       if !self.courses.hasKey(<string>course.course_code) {
           log:printWarn("Invalid course provided for update", course = course);
           return <ValidationError>{
               body: {
                   'error: {
                       code: "INVALID_COURSE",
                       message: "Invalid course"
                   }
               }
           };
       }
 
       log:printInfo("Updating course", course = course);
       self.courses[<string>course.course_code] = course;
       return <CourseUpdated>{};
   }
 
   // Deletes a student
   // return - Deleted student or a validation error
   resource function delete students/[string student_number]() returns Student|ValidationError {
       if !self.students.hasKey(<string>student_number) {
           log:printWarn("Invalid student number to be deleted", student_number = student_number);
           return {
               body: {
                   'error: {
                       code: "INVALID_STUDENT_NUMBER",
                       message: "Invalud student student_number"
                   }
               }
           };
       }
 
       log:printDebug("Deleting student", student_number = student_number);
       Student removed = self.students.remove(student_number);
       log:printDebug("Deleted student", student = removed);
       return removed;
   }
}

//public function main() {
//    io:println("Student Record Management");
//}
